%include "colon.inc"

%include "dict.inc"
%include "lib.inc"

%define BUF_LENGTH 256

section .rodata

%include "words.inc"

err_message: db `Key not found\0`
ov_message: db `Key length exceeds buffer size\0`

section .bss

user_input: resb BUF_LENGTH

section .text
global _start

_start:
    mov rsi, BUF_LENGTH
    mov rdi, user_input
    call read_word
    test rax, rax
    jz .overflow
    mov rdi, last
    mov rsi, rax
    call string_equals
    test rax, rax
    jz .err
    push rax
    call string_length
    pop rdi
    lea rdi, [rdi + rax + 1]
    call print_string
    call exit

.overflow:
    mov rdi, ov_message
    call print_err
    jmp .end

.err:
    mov rdi, err_message
.end:
    call print_err
    call exit
