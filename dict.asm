%include "lib.inc"

section .data
    dictionary_start: dq 0

section .text

global find_word

find_word:
    push rbx
.loop:
    mov rbx, rdi
    lea rdi, [rdi + 8]
    call string_equals
    cmp rax, 1
    je .word_found
    test qword [rbx], rbx
    jz .word_not_found
    jmp .loop

.word_found:
    mov rax, rdi
    pop rbx
    ret

.word_not_found:
    xor rax, rax
    pop rbx
    ret
