ASM = nasm
ASMFLAGS = -felf64 -g
LD = ld
FILES = lib.o dict.o main.o

%.o : %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

build: $(FILES)
	$(LD) -o main $^
	
clean: main
	rm -f *.o main

rebuild:
	make clean
	make build

test: build 
	python3 test.py

.PHONY: build clean rebuild test
