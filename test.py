#!/usr/bin/python3

import subprocess
import unittest

class TestMain(unittest.TestCase):
    def setUp(self):
        self.binary = "./main"

    def execute_binary(self, input_string):
        process = subprocess.Popen([self.binary], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = process.communicate(input_string.encode())
        return output.decode(), error.decode(), process.returncode

    def test_word_in_dictionary(self):
        input_string = "first word"
        expected_output = "first word aaaaaaa"
        output, error, returncode = self.execute_binary(input_string)
        self.assertEqual(returncode, 0)
        self.assertIn(expected_output, output)

    def test_word_not_in_dictionary(self):
        input_string = "nonexistent word"
        expected_error = "Key not found"
        output, error, returncode = self.execute_binary(input_string)
        self.assertEqual(returncode, 0)
        self.assertIn(expected_error, error)

    def test_word_length_exceeds_buffer_size(self):
        input_string = "a" * 257  # Buffer size is 256
        expected_error = "Key length exceeds buffer size"
        output, error, returncode = self.execute_binary(input_string)
        self.assertEqual(returncode, 0)
        self.assertIn(expected_error, error)

if __name__ == "__main__":
    unittest.main()
