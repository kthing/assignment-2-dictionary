section .text
%define NEW_LINE 0xA
%define SYS_WRITE 1
%define FD_STDOUT 1
%define FD_STDERR 2
%define SYS_EXIT 60
%define MINUS_CHAR 0x2D
%define ZERO_CHAR 0x30
%define NINE_CHAR 0x39


global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line
global print_err

 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    xor rdi, rdi
    syscall 
    
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .done
    inc rax
    jmp .loop
.done:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rax, SYS_WRITE 
    mov rdi, FD_STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push di
    mov rax, SYS_WRITE
    mov di, FD_STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop di
    ret
    

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE
    jmp print_char

print_err:
	mov rsi, FD_STDERR


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi            
    mov rdi, 10             
    mov rsi, rsp            
    dec rsp                 
    mov byte [rsp], byte 0       
.loop:
    xor rdx, rdx            
    div rdi                 
    add rdx, ZERO_CHAR           
    dec rsp     
    mov byte [rsp], dl            
    test rax, rax          
    jnz .loop      
             
    mov rdi, rsp            
    push rsi               
    call print_string       
    pop rsp                 
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi	
    jns print_uint	
    push rdi		
    mov rdi, MINUS_CHAR	
    call print_char	
    pop rdi		
    neg rdi		
    jmp print_uint 	



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.loop:
    mov al, [rdi]
    cmp al, [rsi]
    jne .false
    test al, al
    je .true
    inc rdi
    inc rsi
    jmp .loop
.true:
    mov rax, 1
    ret
.false: 
    xor rax, rax 
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12                   
    push r13                    
    push r14 
    mov r12, rdi            
    mov r13, rsi                
    xor r14, r14                             
.check_space:
    test r14, r14           
    jnz .end                
.loop:
    cmp r14, r13            
    jae .check_ov        
    call read_char          
    cmp al, ' '            
    je .check_space       
    cmp al, 0x9            
    je .check_space      
    cmp al, 0xA             
    je .check_space       
    test al, al             
    jz .end                 
    mov [r12+r14], al       
    inc r14               
    jmp .loop             
.check_ov: 
    xor rax, rax           
    jmp .return
.end: 
    mov [r12+r14], byte 0   
    mov rax, r12         
    mov rdx, r14           
.return:    
    pop r14                 
    pop r13                 
    pop r12                   
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r8, 10
	xor rax, rax
	xor rcx, rcx
.loop:
	movzx r9, byte [rdi + rcx]
	cmp r9b, ZERO_CHAR
	jb .end
	cmp r9b, NINE_CHAR
	ja .end
	xor rdx, rdx
	imul r8
	and r9b, 0xf
	add rax, r9
	inc rcx
    cmp r15, 20     
    je .end

	jmp .loop
.end:
	mov rdx, rcx
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    mov cl, byte [rdi]
    cmp cl, MINUS_CHAR
    jne parse_uint          
    inc rdi     
    call parse_uint
    neg rax
    inc rdx
    ret
 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jge .false
    xor rax, rax
    xor rdx, rdx
.loop:
    mov dl, [rdi + rax]
    test dl, dl
    mov [rsi + rax], dl
    je .end
    inc rax
    jmp .loop
.false:
    xor rax, rax
    ret
.end:
    ret
